#!/bin/sh
set -e

autoreconf --verbose --force --install
./configure --with-openssl --with-tpm2
make -j4 check VERBOSE=1
